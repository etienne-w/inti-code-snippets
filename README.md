# INTI code snippets

Rassemble des extraits de code organisés par langage et par sujet. Les extraits sont rangés par langage. Maintenu par la promo INTI Java / Cobol 2023.

## Java

### BDD

- [Configuration BDD locale Spring Boot](./java/db/config/database.properties/) 
- [Configuration Spring MyBatis](./java/db/config/spring-mybatis/) 
- [Configuration Spring Data JPA](./java/db/config/spring-data/) 


## JavaScript

- [Trier les colonnes d'un tableau côté client](js/tables/) 
- [SVG masks and linear gradients to make a 'starred' notation](js/svg-mask) 

const color1 = '#dd7878'
const color2 = '#acb0be'

const div = document.getElementById('snailNote')
const note = div.textContent

div.innerText = ''

for (let i = 0; i < 5; i++) {
	if (i <= note) {
		if (note < i+1) {
			// Fractional part
			snailDiv((note - i) * 100)
		} else {
		// Filled part
			snailDiv(100)
		}
	} else {
		// Empty part
		snailDiv(0)
	}
}

function snailDiv(percentage) {
	const maskDiv = document.createElement('div')
	maskDiv.classList.add('test')
	const child = document.createElement('div') 
	child.setAttribute('style', 'height: 100%; width: 100%; background: linear-gradient(to right, ' + color1 + ' ' + percentage + '%, ' + color2 + ' ' + percentage + '%)')
	maskDiv.appendChild(child)
	div.appendChild(maskDiv)
}

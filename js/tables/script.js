function get(object, properties) {
	return properties.split('.').reduce(
		(obj, x) => (typeof obj == 'undefined' || obj === null) ? obj : obj[x],
		object
	);
}

function reorder(parent, property, compare) {
	var array = parent.children;
	[].slice.call(array).sort(
		(a, b) => compare(get(a, property), get(b, property))
	).forEach(
		(val, index) => parent.appendChild(val)
	);
}

function numCmp(a, b) {
	return a - b
}

function strCmp(a, b) {
	return a.localeCompare(b)
}

function sortButton(rows, button, property, compare) {
	button.classList.add('fas');
	button.classList.add('fa-sort-up');
	var localCmp = compare;
	button.onclick = () => {
		const cls = button.classList;
		reorder(rows, property, localCmp);
		if (cls.contains('fa-sort-up')) {
			button.classList.remove('fa-sort-up');
			button.classList.add('fa-sort-down');
			localCmp = (a, b) => - compare(a, b);
		} else if (cls.contains('fa-sort-down')) {
			button.classList.remove('fa-sort-down');
			button.classList.add('fa-sort-up');
			localCmp = (a, b) => compare(a, b);
		}
	}
}

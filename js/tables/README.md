# Trier les lignes d'un tableau côté client

*very hacky*... Permet d'ajouter des flèches pour ordonner un tableau selon les colonnes.

![Démo](./demo.webm) 

## Dépendances

- Font awesome doit être importé
- Des données sont rangées dans un `<table>`

## Utilisation

- Importer le script [script.js](./script.js) 
- Dans le `<thead>` placer des balises `<i id="someID"></i>` dans les titres des colonnes
- Placer dans la page une balise `<script>` qui:
    - Récupère le `<tbody>` (typiquement: `rows = document.getElementsByTagName('tbody')[0]`)
    - Récupère les id des icones (typiquement: `icon = document.getElementById('someID')`)
    - Appelle la fonction `sortButton(rows, icon, 'children.0.textContent', strCmp)` 
    
## API

Tout est géré par la fonction `sortButton` qui prend 4 arguments:

1. L'objet `tbody` où sont placées les valeurs 
2. L'objet `button` pour placer l'icone font awesome, une balise `<i>` avec un ID.
3. La propriété contre laquelle on va trier. Il faut donner un chemin depuis une ligne du tableau vers une valeur, séparé par des points. Typiquement, pour récupérer le contenu de la colonne N on écrit `'children.N.textContent'`.
4. Une fonction de tri. J'en fournis 2 par défaut: `strCmp` pour les chaînes de caractères et `numCmp` pour les valeurs numériques.

package XXXX;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@PropertySource({ "classpath:database.properties" })
@ComponentScan("XXXX")
@EnableJpaRepositories(basePackages = { "XXXXX" })
@EnableTransactionManagement
public class SpringDataConfig {

  @Autowired
  private Environment environment;

  final public Properties hibernateProps() {
    final Properties props = new Properties();

    props.setProperty("hibernate.dialect",
        environment.getProperty("hibernate.dialect"));
    props.setProperty("hibernate.show_sql",
        environment.getProperty("hibernate.show_sql"));
    props.setProperty("hibernate.hbm2ddl.auto",
        environment.getProperty("hibernate.hbm2ddl.auto"));
    props.setProperty("hibernate.cache.use_second_level_cache",
        environment.getProperty("hibernate.cache.use_second_level_cache"));
    props.setProperty("hibernate.cache.use_query_cache",
        environment.getProperty("hibernate.cache.use_query_cache"));

    return props;
  }

  @Bean
  public DataSource dataSource() {
    final DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName(environment.getProperty("jdbc.driver"));
    dataSource.setUrl(environment.getProperty("jdbc.url"));
    dataSource.setUsername(environment.getProperty("jdbc.user"));
    dataSource.setPassword(environment.getProperty("jdbc.pass"));

    return dataSource;
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    vendorAdapter.setGenerateDdl(true);

    final LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
    entityManagerFactory.setDataSource(dataSource());
    entityManagerFactory.setPackagesToScan("XXXXX");
    entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
    entityManagerFactory.setJpaProperties(hibernateProps());

    return entityManagerFactory;
  }

  @Bean
  public PlatformTransactionManager transactionManager(final EntityManagerFactory emf) {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(emf);
    return transactionManager;
  }
}
